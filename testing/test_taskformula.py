# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/taskformula.py'''

import copy

import pytest
from libtaskotron import taskformula
import libtaskotron.exceptions as exc


class TestGetKeyOrAttribute(object):
    def test_dictionary(self):
        variables = {"mydict": {"foo": {"bar": "moo"}}}
        res = taskformula._get_key_or_attribute(variables, "mydict")
        assert res == variables['mydict']
        res = taskformula._get_key_or_attribute(variables, "mydict.foo")
        assert res == variables['mydict']['foo']
        res = taskformula._get_key_or_attribute(variables, "mydict.foo.bar")
        assert res == variables['mydict']['foo']['bar']

    def test_object(self):
        variables = {"number": 2+1j}
        res = taskformula._get_key_or_attribute(variables, "number")
        assert res == variables['number']
        res = taskformula._get_key_or_attribute(variables, "number.real")
        assert res == variables['number'].real
        res = taskformula._get_key_or_attribute(variables, "number.real.imag")
        assert res == variables['number'].real.imag

    def test_mixed(self):
        variables = {"mydict": {"number": 2+1j}}
        res = taskformula._get_key_or_attribute(variables, "mydict")
        assert res == variables['mydict']
        res = taskformula._get_key_or_attribute(variables, "mydict.number")
        assert res == variables['mydict']['number']
        res = taskformula._get_key_or_attribute(variables, "mydict.number.real")
        assert res == variables['mydict']['number'].real

    def test_exceptions(self):
        with pytest.raises(KeyError):
            taskformula._get_key_or_attribute({}, "foo")
        with pytest.raises(KeyError):
            taskformula._get_key_or_attribute({"foo": {}}, "foo.bar")
        with pytest.raises(AttributeError):
            taskformula._get_key_or_attribute({"foo": None}, "foo.bar")

class TestReplaceVarsInAction(object):

    def setup_method(self, method):
        self.action = {
            'name': 'run foo${foo}',
            'export': '${export}',
            'python': {
                'file': 'somefile.py',
                'args': ['arg1', '${number}', 'arg2', '${list}', '${dict}'],
                'cmdline': '-s ${foo} -i ${number}',
                'nest': {
                    'deepnest': '${dict}',
                    'null': '${null}',
                },
                'from_dict': '${dict.pi}',
                'from_object': '${number.imag}',
                'from_mixed': '${dict.pi.imag}',
            }
        }
        self.vars = {
            'foo': 'bar',
            'export': 'a:b\nc:d\n-e',
            'number': 1,
            'list': [1, 'item'],
            'dict': {'key': 'val', 'pi': 3.14},
            'null': None,
            'extra': 'juicy',
        }

    def test_complex(self):
        rend_action = copy.deepcopy(self.action)
        rend_action['name'] = 'run foo%s' % self.vars['foo']
        rend_action['export'] = self.vars['export']
        rend_action['python']['args'] = ['arg1', self.vars['number'], 'arg2',
                                         self.vars['list'], self.vars['dict']]
        rend_action['python']['cmdline'] = '-s %s -i %s' % (self.vars['foo'],
                                                            self.vars['number'])
        rend_action['python']['nest']['deepnest'] = self.vars['dict']
        rend_action['python']['nest']['null'] = self.vars['null']
        rend_action['python']['from_dict'] = self.vars['dict']['pi']
        rend_action['python']['from_object'] = self.vars['number'].imag
        rend_action['python']['from_mixed'] = self.vars['dict']['pi'].imag

        taskformula.replace_vars_in_action(self.action, self.vars)
        assert self.action == rend_action

    def test_raise(self):
        with pytest.raises(exc.TaskotronYamlError):
            taskformula.replace_vars_in_action(self.action, {})

        del self.vars['number']
        with pytest.raises(exc.TaskotronYamlError):
            taskformula.replace_vars_in_action(self.action, self.vars)


class TestReplaceVars(object):

    def test_standadlone_var_str(self):
        res = taskformula._replace_vars('${foo}', {'foo': 'hedgehog'})
        assert res == 'hedgehog'

    def test_standalone_var_nonstr(self):
        res = taskformula._replace_vars('${foo}', {'foo': ['chicken', 'egg']})
        assert res == ['chicken', 'egg']

    def test_standadlone_var_str_alternative(self):
        res = taskformula._replace_vars('$foo', {'foo': 'hedgehog'})
        assert res == 'hedgehog'

    def test_standalone_var_nonstr_alternative(self):
        res = taskformula._replace_vars('$foo', {'foo': ['chicken', 'egg']})
        assert res == ['chicken', 'egg']

    def test_multivar(self):
        res = taskformula._replace_vars('a ${b} c${d}', {'b': 'bb', 'd': 'dd'})
        assert res == 'a bb cdd'

    def test_empty_space(self):
        '''Some empty space around a single variable is like multi-var, i.e.
        a string is always returned'''
        res = taskformula._replace_vars(' ${foo}', {'foo': 'bar'})
        assert res == ' bar'

        res = taskformula._replace_vars(' ${foo}', {'foo': 42})
        assert res == ' 42'

    def test_recursive_content(self):
        '''The content of a variable contains more variable names. But those
        must not be expanded.'''
        text = '${foo} ${bar}'
        varz = {'foo': '${foo} ${bar}',
                'bar': '${foo} ${bar} ${baz}'
               }
        res = taskformula._replace_vars(text, varz)
        assert res == '%s %s' % (varz['foo'], varz['bar'])

    def test_empty(self):
        res = taskformula._replace_vars('', {'extra': 'var'})
        assert res == ''

    def test_same_var(self):
        '''Only variables must get replaced, not same-sounding text'''
        res = taskformula._replace_vars('foo ${foo}', {'foo': 'bar'})
        assert res == 'foo bar'

    def test_dot_selector(self):
        res = taskformula._replace_vars('foo ${foo.bar}', {'foo': {'bar': 'moo'}})
        assert res == 'foo moo'
        res = taskformula._replace_vars('foo ${foo.bar.imag}', {'foo': {'bar': 1}})
        assert res == 'foo 0'
        res = taskformula._replace_vars('foo $foo.bar', {'foo': {'bar': 'moo'}})
        assert res == 'foo moo'
        res = taskformula._replace_vars('foo $foo.bar.imag', {'foo': {'bar': 1}})
        assert res == 'foo 0'

    def test_combined_syntax(self):
        '''Both $foo and ${foo} syntax must work, together with escaping'''
        res = taskformula._replace_vars('foo $foo ${foo} $$foo', {'foo': 'bar'})
        assert res == 'foo bar bar $foo'

    def test_raise_missing_var(self):
        with pytest.raises(exc.TaskotronYamlError):
            taskformula._replace_vars('${foo}', {'bar': 'foo'})

    def test_raise_missing_vars(self):
        with pytest.raises(exc.TaskotronYamlError):
            taskformula._replace_vars('${foo} ${bar}', {'bar': 'foo'})

    def test_raise_bad_escape(self):
        '''Dollars must be doubled'''
        with pytest.raises(exc.TaskotronYamlError):
            taskformula._replace_vars('${foo} $', {'foo': 'bar'})

    def test_raise_bad_selector(self):
        '''Selectors must point to existing properties'''
        with pytest.raises(exc.TaskotronYamlError):
            taskformula._replace_vars('${foo.bar}', {'foo': None})
        with pytest.raises(exc.TaskotronYamlError):
            taskformula._replace_vars('${foo.bar}', {'foo': {}})

class TestDeviseEnvironment(object):
    def test_raise_invalid_env(self):
        with pytest.raises(exc.TaskotronValueError):
            taskformula.devise_environment({'environment': ''}, None)

    def test_empty_env_no_infer_data(self):
        res = taskformula.devise_environment({}, {})
        assert res == {'distro': None, 'release': None, 'flavor': None, 'arch': None}

    def test_env_defined_no_infer_needed(self):
        env = {'distro': 'rhel',
               'release': 'el7.5',
               'flavor': 'awesome_albatros',
               'arch': 'i386'}
        ret = taskformula.devise_environment({'environment': env}, {})
        assert ret == env

    def test_infer_from_koji_build(self):
        arg_data = {"type": "koji_build", "item": "xchat-2.8.8-21.fc20"}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': 'fedora', 'release': '20', 'flavor': None, 'arch': None}

    def test_infer_from_koji_build_unknown_distro(self):
        arg_data = {"type": "koji_build", "item": "xchat-2.8.8-21.XY20"}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': None, 'release': None, 'flavor': None, 'arch': None}

    def test_infer_from_koji_tag(self):
        arg_data = {"type": "koji_tag", "item": "f23-updates-pending"}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': 'fedora', 'release': '23', 'flavor': None, 'arch': None}

    def test_infer_from_koji_tag_unknown_distro(self):
        arg_data = {"type": "koji_tag", "item": "el6-updates-pending"}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': None, 'release': None, 'flavor': None, 'arch': None}

    def test_infer_from_arch(self):
        arg_data = {"arch": []}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': None, 'release': None, 'flavor': None, 'arch': None}

        arg_data = {"arch": ['noarch']}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': None, 'release': None, 'flavor': None, 'arch': None}

        arg_data = {"arch": ['i386']}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': None, 'release': None, 'flavor': None, 'arch': 'i386'}

    def test_infer_from_multiple_arches(self):
        arg_data = {"arch": ['i386', 'noarch']}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': None, 'release': None, 'flavor': None, 'arch': 'i386'}

        arg_data = {"arch": ['i386', 'x86_64']}
        ret = taskformula.devise_environment({'environment': {}}, arg_data)
        assert ret == {'distro': None, 'release': None, 'flavor': None, 'arch': None}
