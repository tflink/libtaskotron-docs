# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/directives/bodhi_directive.py'''

import pytest
from dingus import Dingus

from libtaskotron.directives import bodhi_directive
from libtaskotron.exceptions import TaskotronDirectiveError

class TestBodhiDownloads():

    def setup_method(self, method):
        '''Run this before every test invocation'''
        self.ref_arch = ['x86_64']
        self.ref_update_id = 'FEDORA-1234-56789'
        self.ref_workdir = '/var/tmp/foo'
        self.ref_taskfile = '/foo/bar/task.yml'
        self.ref_nvr1 = 'foo-1.2.3.fc99'
        self.ref_nvr2 = 'bar-1.2.3.fc99'
        self.ref_update = {'title': 'Random update',
                       'builds': [{'nvr': self.ref_nvr1, 'package': {}},
                                  {'nvr': self.ref_nvr2, 'package': {}}
                                  ],
                       'other_keys': {}
                       }
        self.ref_input = {'action': 'download',
                          'arch': self.ref_arch,
                          'update_id': self.ref_update_id
                         }
        self.ref_arg_data = {'workdir': self.ref_workdir}
        self.ref_rpmfile = '%s/%s.rpm' % (self.ref_workdir, self.ref_nvr1)

        self.helper = bodhi_directive.BodhiDirective()


    def test_action_download_existing_update(self):
        '''Test download of existing update'''
        stub_bodhi = Dingus(get_update__returns=self.ref_update)
        stub_koji = Dingus(get_nvr_rpms__returns=[self.ref_rpmfile])

        test_bodhi = bodhi_directive.BodhiDirective(stub_bodhi, stub_koji)

        output_data = test_bodhi.process(self.ref_input, self.ref_arg_data)

        ref_output_data = {'downloaded_rpms': [self.ref_rpmfile,
                                               self.ref_rpmfile]}
        getrpms_calls = stub_koji.calls()
        requested_nvrs = map(lambda x: x[1][0], getrpms_calls)

        # checks whether get_nvr_rpms was called for every update
        # with correct nvrs
        assert len(getrpms_calls) == 2
        assert requested_nvrs == [self.ref_nvr1, self.ref_nvr2]
        assert output_data == ref_output_data


    def test_action_download_all_archs(self):
        '''Test download when all arches are demanded'''
        self.ref_arch = ['all']
        self.ref_input = {'action': 'download',
                          'arch': self.ref_arch,
                          'update_id': self.ref_update_id
                          }

        stub_bodhi = Dingus(get_update__returns=self.ref_update)
        stub_koji = Dingus(get_nvr_rpms__returns=[self.ref_rpmfile])

        test_bodhi = bodhi_directive.BodhiDirective(stub_bodhi, stub_koji)

        test_bodhi.process(self.ref_input, self.ref_arg_data)

        ref_arches = set(['i386', 'x86_64', 'noarch', 'armhfp'])
        getrpms_calls = stub_koji.calls()
        req_arches = map(lambda x: x[1][2], getrpms_calls)

        # checks whether all get_nvr_rpms calls demanded all arches
        assert all(map(lambda x: set(x) == ref_arches, req_arches))

    def test_action_download_source(self):
        '''Test download of source packages'''
        self.ref_arch = []
        self.ref_input = {'action': 'download',
                          'arch': self.ref_arch,
                          'update_id': self.ref_update_id,
                          'src': True
                          }

        stub_bodhi = Dingus(get_update__returns=self.ref_update)
        stub_koji = Dingus(get_nvr_rpms__returns=[self.ref_rpmfile])

        test_bodhi = bodhi_directive.BodhiDirective(stub_bodhi, stub_koji)

        test_bodhi.process(self.ref_input, self.ref_arg_data)

        getrpm_calls = stub_koji.calls()

        # checks whether all get_nvr_rpms calls demanded only source pkgs
        assert all(map(lambda x: x[1][2] == [], getrpm_calls))
        assert all(map(lambda x: x[2]['src'], getrpm_calls))

    def test_action_download_multiple_arch(self):
        '''Test download of multiple arches packages'''
        self.ref_arch = ['x86_64', 'noarch']
        self.ref_input = {'action': 'download',
                          'arch': self.ref_arch,
                          'update_id': self.ref_update_id
                          }

        stub_bodhi = Dingus(get_update__returns=self.ref_update)
        stub_koji = Dingus(get_nvr_rpms__returns=[self.ref_rpmfile])

        test_bodhi = bodhi_directive.BodhiDirective(stub_bodhi, stub_koji)

        test_bodhi.process(self.ref_input, self.ref_arg_data)

        getrpms_calls = stub_koji.calls()
        req_arches = map(lambda x: x[1][2], getrpms_calls)

        # checks whether all get_nvr_rpms calls demanded only source pkgs
        assert all(map(lambda x: x == ['x86_64', 'noarch'], req_arches))

    def test_action_download_added_noarch(self):
        '''Test whether noarch is automaticaly added'''
        self.ref_arch = ['i386']
        self.ref_input = {'action': 'download',
                          'arch': self.ref_arch,
                          'update_id': self.ref_update_id
        }

        stub_bodhi = Dingus(get_update__returns=self.ref_update)
        stub_koji = Dingus(get_nvr_rpms__returns=[self.ref_rpmfile])

        test_bodhi = bodhi_directive.BodhiDirective(stub_bodhi, stub_koji)

        test_bodhi.process(self.ref_input, self.ref_arg_data)

        getrpms_calls = stub_koji.calls()
        req_arches = map(lambda x: x[1][2], getrpms_calls)

        # checks whether noarch is demanded in all get_nvr_rpms calls
        assert all(map(lambda x: 'noarch' in x, req_arches))


    def test_action_download_nonexisting_update(self):
        '''Test whether exception is raised when no update is found'''
        stub_bodhi = Dingus(get_update__returns=None)
        stub_koji = Dingus(get_nvr_rpms__returns="It shouldn't go this far")

        test_bodhi = bodhi_directive.BodhiDirective(stub_bodhi, stub_koji)

        # No update is found, b. directive should raise an Exception
        with pytest.raises(TaskotronDirectiveError):
            test_bodhi.process(self.ref_input, self.ref_arg_data)


    def test_invalid_action(self):
        '''Test response on non-existing action'''

        self.ref_input['action'] = 'foo'

        stub_bodhi = Dingus()
        stub_koji = Dingus()

        test_bodhi = bodhi_directive.BodhiDirective(stub_bodhi, stub_koji)

        # Unknown action should raise an Exception
        with pytest.raises(TaskotronDirectiveError):
            test_bodhi.process(self.ref_input, self.ref_arg_data)

    def test_action_download_insufficient_params(self):
        '''Test response on unsufficient input for download action'''
        ref_input = {'action': 'download'}

        stub_bodhi = Dingus()
        stub_koji = Dingus()

        test_bodhi = bodhi_directive.BodhiDirective(stub_bodhi, stub_koji)

        # Tests with no information
        with pytest.raises(TaskotronDirectiveError):
            test_bodhi.process(ref_input, self.ref_arg_data)

        ref_input['arch'] = self.ref_arch

        # Tests with only arch, or update_id given
        with pytest.raises(TaskotronDirectiveError):
            test_bodhi.process(ref_input, self.ref_arg_data)

        ref_input.pop('arch')
        ref_input['update_id'] = self.ref_update_id

        with pytest.raises(TaskotronDirectiveError):
            test_bodhi.process(ref_input, self.ref_arg_data)
