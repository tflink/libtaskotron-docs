# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import os
import copy
from libtaskotron.directives import python_directive


class TestTaskMethod(object):
    def setup_method(self, method):
        self.ref_taskyaml = 'footask.yml'
        self.ref_pyfile = 'some_loadable_test.py'
        self.ref_env = {'task': os.path.join(os.path.dirname(
            os.path.abspath(__file__)), self.ref_taskyaml)}
        self.test_directive = python_directive.PythonDirective()

    def testfunc_task_method(self):
        ref_input = {'file': self.ref_pyfile, 'callable': 'task_method'}

        test_output = self.test_directive.process(copy.copy(ref_input),
                                                  self.ref_env)

        assert test_output == "I'm a task method!"

    def testfunc_class_embedded_call(self):
        ref_input = {'file': self.ref_pyfile,
                     'callable': 'embedded_task_target'}

        test_output = self.test_directive.process(copy.copy(ref_input),
                                                  self.ref_env)

        assert test_output == "I'm a __call__ method in a class!"

    def testfunc_class_target(self):
        ref_input = {'file': self.ref_pyfile, 'callable': 'task_class_target'}

        test_output = self.test_directive.process(copy.copy(ref_input),
                                                  self.ref_env)

        assert test_output == "I'm a task class method!"
