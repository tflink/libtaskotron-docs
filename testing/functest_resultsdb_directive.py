# -*- coding: utf-8 -*-
# Copyright 2009-2016, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import pytest
from dingus import Dingus
import configparser

from libtaskotron.directives import resultsdb_directive

from libtaskotron import check
from libtaskotron import config


class StubConfigParser(object):
    def read(self, *args):
        pass

    def __getitem__(self, key):
        return {'url': 'giturl/testname'}


@pytest.mark.usefixtures('setup')
class TestResultsdbDirective():
    @pytest.fixture
    def setup(self, monkeypatch):
        '''Run this before every test invocation'''

        self.cd = check.CheckDetail(
            item='foo_bar',
            report_type=check.ReportType.KOJI_BUILD,
            outcome='NEEDS_INSPECTION',
            note='foo_bar note',
            output=["foo\nbar"],
            keyvals={"foo": "moo1", "bar": "moo2"},
            )

        self.yaml = check.export_YAML(self.cd)

        self.ref_input = {'results': self.yaml}
        self.ref_arg_data = {
            'resultsdb_job_id': 1,
            'checkname': 'test_resultsdb_report',
            'namespace': 'qa',
            'jobid': 'all/123',
            'uuid': 'c25237a4-b6b3-11e4-b98a-3c970e018701',
            'artifactsdir': '/some/directory/',
            'task': '/taskdir',
            'item': 'firefox-45.0.2-1.fc23'
            }

        self.ref_resultdata = {u'id': 1234}

        self.ref_jobid = 1234
        self.ref_refurl = u'http://example.com/%s' % self.ref_arg_data['checkname']
        self.ref_jobdata = {u'end_time': None,
                            u'href': u'http://127.0.0.1/api/v1.0/jobs/%d' % self.ref_jobid,
                            u'id': self.ref_jobid,
                            u'name': self.ref_arg_data['checkname'],
                            u'ref_url': self.ref_refurl,
                            u'results': [],
                            u'results_count': 0,
                            u'start_time': None,
                            u'status': u'SCHEDULED'}

        self.stub_rdb = Dingus('resultsdb', get_testcase__returns={},
                               create_job__returns=self.ref_jobdata,
                               create_result__returns=self.ref_resultdata,
                               )
        self.test_rdb = resultsdb_directive.ResultsdbDirective(self.stub_rdb)

        # while it appears useless, this actually sets config in several tests
        monkeypatch.setattr(config, '_config', None)
        self.conf = config.get_config()
        self.conf.report_to_resultsdb = True

        monkeypatch.setattr(configparser, 'ConfigParser', StubConfigParser)

    def test_param_file(self, tmpdir):
        del self.ref_input['results']
        input_file = tmpdir.join('results.yaml')
        input_file.write(self.yaml)
        self.ref_input['file'] = input_file.strpath

        self.test_rdb.process(self.ref_input, self.ref_arg_data)

        # Given the input data, the resultsdb should be called once, and only
        #   once, calling "create_result".
        # This assert failing either means that more calls were added in the
        #   source code, or that a bug is present, and "create_result" is
        #   called multiple times.
        # we expect rdb to be called 4 times: create job, update to RUNNING,
        # check for testcase, report result and complete job

        assert len(self.stub_rdb.calls()) == 5

        # Select the first call of "create_result" method.
        # This could be written as self.stub_rdb.calls()[0] at the moment, but
        #   this is more future-proof, and accidental addition of resultsdb
        #   calls is handled by the previous assert.
        call = [call for call in self.stub_rdb.calls() if call[0] == 'create_result'][0]
        # Select the keyword arguments of that call
        call_data = call[2]

        # the log url depends on the arg_data, so construct it here
        ref_builder, ref_jobid = self.ref_arg_data['jobid'].split('/')
        ref_log_url = '%s/builders/%s/builds/%s/steps/%s/logs/stdio' %\
                      (self.conf.taskotron_master, ref_builder, ref_jobid,
                       self.conf.buildbot_task_step)
        ref_testcase_name = '%s.%s' % (self.ref_arg_data['namespace'],
                                       self.ref_arg_data['checkname'])

        assert call_data['job_id'] == self.ref_jobid
        assert call_data['testcase_name'] == ref_testcase_name
        assert call_data['outcome'] == self.cd.outcome
        assert call_data['summary'] == self.cd.note
        assert call_data['log_url'] == ref_log_url
        assert call_data['item'] == self.cd.item
        assert call_data['type'] == self.cd.report_type

        assert 'output' not in call_data.keys()

        for key in self.cd.keyvals.keys():
            assert call_data[key] == self.cd.keyvals[key]
