import imp
import os
import sys
from mock import Mock

import pytest

from libtaskotron import executor
from libtaskotron import exceptions as exc


class TestExecutorDirectiveLoader():

    def setup_method(self, method):
        self.ref_dir = os.path.abspath('testing/test_directives/')
        self.ref_formula = {}
        self.ref_params = {'uuid': ''}
        self.ref_directivename = 'stub'

    def test_executor_load_directive(self, tmpdir):
        test_executor = executor.Executor(self.ref_formula, self.ref_params, tmpdir)

        test_executor._load_directive(self.ref_directivename, self.ref_dir)

        assert '%s_directive' % self.ref_directivename in sys.modules.keys()

    def test_executor_load_directive_indict(self, tmpdir):
        test_executor = executor.Executor(self.ref_formula, self.ref_params, tmpdir)

        test_executor._load_directive(self.ref_directivename, self.ref_dir)

        assert self.ref_directivename in test_executor.directives.keys()

    def test_executor_load_directive_notexist(self, tmpdir):
        self.ref_directivename = 'noexist'

        test_executor = executor.Executor(self.ref_formula, self.ref_params, tmpdir)

        with pytest.raises(exc.TaskotronDirectiveError):
            test_executor._load_directive(self.ref_directivename, self.ref_dir)

    def test_executor_load_directive_our_importerror(self, tmpdir, monkeypatch):
        """ The idea here is to test the logic that looks for an ImportError
        during directive loading, transforms the exception and includes a hint
        to install sub-packages if it's coming from libtaskotron
        """

        stub_imp = Mock(side_effect=(ImportError("No module libtaskotron.stub")))
        monkeypatch.setattr(imp, 'load_source', stub_imp)

        test_executor = executor.Executor(self.ref_formula, self.ref_params, tmpdir)

        with pytest.raises(exc.TaskotronImportError) as e:
            test_executor._load_directive(self.ref_directivename, self.ref_dir)
        assert 'Hint' in e.value.message

    def test_executor_load_directive_notour_importerror(self, tmpdir, monkeypatch):
        """ The similar case as out_importerror just check that the exception message
        does not include a hint
        """
        stub_imp = Mock(side_effect=(ImportError("No module unicorn.candy")))
        monkeypatch.setattr(imp, 'load_source', stub_imp)

        test_executor = executor.Executor(self.ref_formula, self.ref_params, tmpdir)

        with pytest.raises(exc.TaskotronImportError) as e:
            test_executor._load_directive(self.ref_directivename, self.ref_dir)
        assert 'Hint' not in e.value.message
