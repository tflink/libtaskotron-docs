=====================
Taskotron Quick Start
=====================


Install
=======

Install ``libtaskotron-core`` and all of its modules::
  
  sudo dnf -y install libtaskotron

See :ref:`libtaskotron-modules` for more information on installing individual 
modules.

Since the ``runtask`` command writes into system-wide locations on the 
filesystem, any user who runs it will need to be added to the ``taskotron`` 
group with the command::

  sudo usermod -aG taskotron <user>

.. Note::
   The user must log out and back in in order for the new group assignment to 
   take effect.


Run
===

::

  runtask -i <item> -t <type> -a <arch> <yaml formula>

.. note::
   By default, runtask will attempt to execute tasks locally. More command line
   flags are needed in order to enable disposable clients. `More details on
   running tasks can be found here <index.html#running-tasks>`_


Formulae: General Syntax
========================

.. code-block:: yaml

   name: polyjuice potion
   desc: potion that allows to take the form of someone else
   maintainer: hermione

   input:
       args:
           - arg1   # supported args:
           - arg2   # arch, koji_build, bodhi_id, koji_tag

   environment:
       rpm:
           - dependency1   # libtaskotron installs these when in production or --local mode,
           - dependency2   # otherwise users are expected to install them on libtaskotron machine

   actions:
       - name: pick fluxweed on a full moon
         directivename:              # e.g. koji, bodhi, python, ...
             arg1: value1
             arg2: ${some_variable}  # e.g. input arg
         export: firststep_output    # export usable as input arg of next directive

       - name: next step of formula
         nextdirectivename:
          arg1: ${firststep_output}

`Detailed instructions on writing tasks <writingtasks.html#creating-a-new-task>`_


Formulae: Examples From Our Git
===============================

Running Rpmlint
---------------

`Rpmlint <https://bitbucket.org/fedoraqa/task-rpmlint>`_

::

  git clone https://bitbucket.org/fedoraqa/task-rpmlint.git

::

  runtask -i gourmet-0.16.0-2.fc20 -t koji_build -a x86_64 task-rpmlint/runtask.yml


Other Examples
--------------


`Rpmbuild <https://bitbucket.org/fedoraqa/task-rpmbuild>`_

`Example bodhi <https://bitbucket.org/fedoraqa/task-examplebodhi>`_

`Example reporting <https://bitbucket.org/fedoraqa/task-example_reporting>`_

Check `our git <https://bitbucket.org/fedoraqa>`_ for more.
