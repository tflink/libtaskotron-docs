# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Methods for operating with a task formula'''

from __future__ import absolute_import
import collections
import string
import re

from libtaskotron import exceptions as exc
from libtaskotron.logger import log
from libtaskotron.ext.fedora import rpm_utils


class DotTemplate(string.Template):
    """Redefines the :class:`string.Template`'s ``idpattern`` property to
    allow using ``.`` in the variable names. This is then later used for
    key/property access in variable replacement.
    """
    idpattern = '[_a-z](?:\.?[_a-z0-9]+)*'


def _get_key_or_attribute(variables, var_name):
    """Allows for the ``.`` selector to work as selector for variables.
    When a variable is dict-like (collections.MutableMapping), the selector looks for a key.
    When a variable is not dict-like, it looks for the object's attribute.

    :param dict variables: names (keys) and values of available variables
    :param str var_name: variable name, possibly containing ``.`` selector to be found
    :raise KeyError: when a dict-like object (anywhere in the chain) does not contain requested key
    :raise AttributeError: when an non-dic object (anywhere in the chain)
                        does not have the requested attribute
    """
    if '.' not in var_name:
        return variables[var_name]

    var_name, selector = var_name.split('.', 1)
    outcome = variables[var_name]
    for part in selector.split('.'):
        if isinstance(outcome, collections.MutableMapping):
            outcome = outcome[part]
        else:
            outcome = outcome.__getattribute__(part)
    return outcome

def _replace_vars(text, variables):
    '''Go through ``text`` and replace all variables (in the form of what is
    supported by :class:`VariableTemplate`) with their values, as provided in
    ``variables``. This is used for variable expansion in the task formula.

    :param str text: input text where to search for variables
    :param dict variables: names (keys) and values of variables to replace
    :return: if ``text`` contains just a single variable and nothing else, the
             variable value is directly returned (i.e. with matching type, not
             cast to ``str``). If ``text`` contains something else as well
             (other variables or text), a string is returned.
    :raise TaskotronYamlError: if ``text`` contains a variable that is not
                               present in ``variables``, or if the variable
                               syntax is incorrect
    '''

    try:
        # try to find the first match
        match = DotTemplate.pattern.search(text)

        if not match:
            return text

        # There are 4 groups in the pattern: 1 - escaped, 2 - named, 3 - braced,
        # 4 - invalid. Group 0 returns the whole match.
        if match.group(0) == text and (match.group(2) or match.group(3)):
            # We found a single variable and nothing more. We shouldn't return
            # a string, but the exact value, so that we don't lose value type.
            # This makes it possible to pass lists, dicts, etc as variables.
            var_name = match.group(2) or match.group(3)
            return _get_key_or_attribute(variables, var_name)

        # Now it's clear there's also something else in `text` than just a
        # single variable. We will replace all variables and return a string
        # again.

        # To make things easy, we'll create expanded_variables dict, that contains values for
        # all the replacable variables in the text.
        # i.e. if variables = {"n": 1}, and text is "foo bar {$n.imag}" then
        # "n.imag" key is added to variables, with the respective value.

        expanded_variables = {}
        for match in DotTemplate.pattern.findall(text):
            var_name = match[1] or match[2]
            if var_name:
                expanded_variables[var_name] = _get_key_or_attribute(variables, var_name)

        output = DotTemplate(text).substitute(expanded_variables)
        return output

    except (KeyError, AttributeError) as e:
        raise exc.TaskotronYamlError("The task formula includes a variable, "
            "but no value has been provided for it: %s" % e)

    except ValueError as e:
        raise exc.TaskotronYamlError("The task formula includes an incorrect "
            "variable definition. Dollar signs must be doubled if they "
            "shouldn't be considered as a variable denotation.\n"
            "Error: %s\n"
            "Text: %s" % (e, text))


def replace_vars_in_action(action, variables):
    '''Find all variables that are leaves (in a tree sense) in an action. Leaves
    are variables which can no longer be traversed, i.e. "primitive" types like
    ``str`` or ``int``. Non-leaves are containers like ``dict`` or ``list``.

    For all leaves, call :func:`_replace_vars` and update their value with
    the function's output.

    :param dict action: An action specification parsed from the task formula.
                        See :meth:`.Runner.do_actions` to see what an action
                        looks like.
    :param dict variables: names (keys) and values of variables to replace
    :raise TaskotronYamlError: if ``text`` contains a variable that is not
                               present in ``variables``, or if the variable
                               syntax is incorrect
    '''

    visited = []  # all visited nodes in a tree
    stack = [action]  # nodes waiting for inspection

    while stack:
        vertex = stack.pop()

        if vertex in visited:
            continue

        visited.append(vertex)
        children = [] # list of tuples (index/key, child_value)

        if isinstance(vertex, collections.MutableMapping):
            children = vertex.items()  # list of (key, value)
        elif isinstance(vertex, collections.MutableSequence):
            children = list(enumerate(vertex))  # list of (index, value)
        else:
            log.warn("Unknown structure '%s' in YAML file, this shouldn't "
                     "happen: %s", type(vertex), vertex)

        for index, child_val in children:
            if isinstance(child_val, basestring):
                # leaf node and a string, replace variables
                vertex[index] = _replace_vars(child_val, variables)
            elif isinstance(child_val, collections.Iterable):
                # traversable further down, mark for visit
                stack.append(child_val)


def devise_environment(formula, arg_data):
    '''Takes a parsed formula, and returns a required run-environment (i.e.
    distro, arch, fedora release, and base-image flavor), based on item and type.

    :param dict formula: parsed formula file (or dict with equivalent structure)
    :param dict arg_data: parsed command-line arguments. item, type and arch
                          are used in this method
    :return: dict containing release, flavor, arch. Each either set, or None
    :raise TaskotronValueError: when environment can't be parsed from the formula
    '''

    environment = formula.get('environment', {})
    env = {'distro': None, 'release': None, 'flavor': None, 'arch': None}

    if not isinstance(environment, dict):
        raise exc.TaskotronValueError(
            "Environment must be dictionary, not %s" % type(environment))

    env['distro'] = environment.get('distro', None)
    env['release'] = environment.get('release', None)
    env['flavor'] = environment.get('flavor', None)
    env['arch'] = environment.get('arch', None)

    item = arg_data.get('item', None)
    item_type = arg_data.get('type', None)

    if not env['distro']:
        if item_type == 'koji_build':
            # FIXME: find a way to make this not Fedora-specific
            # For `xchat-2.8.8-21.fc20` disttag is `fc20` for example
            try:
                distro = rpm_utils.get_dist_tag(item)[:2]
                env['distro'] = {'fc': 'fedora'}.get(distro, None)
            except exc.TaskotronValueError:
                env['distro'] = None

        elif item_type == 'koji_tag':
            if re.match(r'^f[0-9]{2}-.*', item):
                env['distro'] = 'fedora'

        if env['distro']:
            log.debug("Environment/distro overriden with ENVVAR from %r:%r to %r" %
                      (item_type, item, env['distro']))
        else:
            log.debug("Environment/distro can not be inferred from %r:%r. Using default." %
                      (item_type, item))

    if not env['release']:
        if item_type == 'koji_build':
            # FIXME: find a way to make this not Fedora-specific
            # Last two characters in rpm's disttag are the Fedora release.
            # For `xchat-2.8.8-21.fc20` disttag is `fc20` for example
            try:
                env['release'] = rpm_utils.get_dist_tag(item)[-2:]
            except exc.TaskotronValueError:
                env['release'] = None

        elif item_type == 'koji_tag':
            if re.match(r'^f[0-9]{2}-.*', item):
                env['release'] = item[1:3]

        if env['release']:
            log.debug("Environment/release inferred from %r:%r to %r" %
                      (item_type, item, env['release']))
        else:
            log.debug("Environment/release can not be inferred from %r:%r. Using default." %
                      (item_type, item))

    if not env['flavor']:
        log.debug("Environment/flavor not specified. Using default")

    if not env['arch']:
        arches = [i for i in arg_data.get('arch', []) if i != 'noarch']

        if arches and len(arches) == 1:
            env['arch'] = arches[0]
            log.debug("Environment/arch inferred from %r to %r" %
                      (arg_data.get('arch', []), env['arch']))
        else:
            log.warn("Environment/arch can not be inferred from %r. Using default." %
                     arg_data.get('arch', []))

    return env
