# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import

DOCUMENTATION = """
module: distgit_directive
short_description: download files from distgit
description: |
  Download files from Fedora package repository (usually called 'distgit') hosted at
  http://pkgs.fedoraproject.org/. Any files hosted at that repository can be downloaded for a
  chosen package.
parameters:
  package:
    required: true
    description: |
      N(E)VR of a package. Package dist tag determines the git branch used for file download.
      Example: ``xchat-2.8.8-21.fc20``
    type: str
  path:
    required: true
    description: |
      files (directories not supported at the moment) to be downloaded from distgit.
      Example: ``[xchat.spec]``
    type: list of str
  localpath:
    required: false
    description: |
      a local path of downloaded file. If not provided, path from distgit will be used.
      Example: ``[specs/xchat.spec]``
    type: list of str
  target_dir:
    required: false
    description: directory into which to download files
    type: str
    default: ${workdir}
returns: |
  A dictionary containing following items:

  * `downloaded_files`: (list of str) a list of local filenames of the downloaded
    files
raises: |
  * :class:`.TaskotronRemoteError`: if downloading failed
  * :class:`.TaskotronValueError`: if path and localpath are not lists or are not of the same
    length
  * :class:`.TaskotronDirectiveError`: if package or path is missing
version_added: 0.3.16
"""

EXAMPLES = """
A task needs to download a spec file and httpd configuration check to run a check
on those files::

  - name: download spec file and httpd conf
    distgit:
      package: yourls-1.7-3.20150410gitabc7d6c.fc22
      path:
          - yourls.spec
          - yourls-httpd.conf
      localpath:
          - download/yourls_downloaded.spec
          - download/yourls-httpd_downloaded.conf
"""

import os.path

from libtaskotron.directives import BaseDirective
from libtaskotron import file_utils, python_utils
from libtaskotron.ext.fedora import rpm_utils

import libtaskotron.exceptions as exc



directive_class = 'DistGitDirective'

URL_FMT = 'http://pkgs.fedoraproject.org/cgit/%s.git/plain/%s?h=%s'


class DistGitDirective(BaseDirective):
    def __init__(self):
        super(DistGitDirective, self).__init__()

    def _download_file(self, package, path, localpath):
        pkgname = rpm_utils.rpmformat(package, fmt='n')
        dist_tag = rpm_utils.get_dist_tag(package)
        # fc22 -> f22 conversion, since that's how branches are called
        branch = dist_tag.replace('c', '')
        url = URL_FMT % (pkgname, path, branch)

        return file_utils.download(url, '.', localpath)

    def process(self, params, arg_data):
        if 'package' not in params or 'path' not in params:
            detected_args = ', '.join(params.keys())
            raise exc.TaskotronDirectiveError(
                "The distgit directive requires 'package' and 'path' arguments."
                "Detected arguments: %s" % detected_args)

        if 'target_dir' not in params:
            target_dir = arg_data['workdir']
        else:
            target_dir = params['target_dir']

        if not python_utils.iterable(params['path']):
            raise exc.TaskotronValueError("Incorrect value type of the 'path' argument: "
                                          "%s" % type(params['path']))

        target_path = params['path']
        output_data = {}

        if 'localpath' in params:
            if not python_utils.iterable(params['localpath']):
                raise exc.TaskotronValueError("Incorrect value type of the 'localpath' argument: "
                                              "%s" % type(params['path']))

            if not len(params['path']) == len(params['localpath']):
                raise exc.TaskotronValueError('path and localpath lists must be of the same '
                                              'length.')

            target_path = params['localpath']


        output_data['downloaded_files'] = []
        for path, localpath in zip(params['path'], target_path):
            localpath = os.path.join(target_dir, localpath)
            file_utils.makedirs(os.path.dirname(localpath))
            output_data['downloaded_files'].append(
                self._download_file(params['package'], path, localpath)
            )

        return output_data
