# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import

DOCUMENTATION = """
module: mash_directive
short_description: create a YUM repository from RPM packages using mash
description: |
  Take a directory containing RPM packages and run ``mash`` command on it,
  which creates YUM repository metadata.

  This internally calls ``createrepo`` (which can be used separately through the
  :ref:`createrepo directive <createrepo_directive>`), and then ``mash`` to
  finish the process. Mash will handle creating delta RPMs (if requested)
  and it will handle creating multilib repository metadata (which is currently
  supported just for ``x86_64`` architecture).

  .. note:: Beware, when creating multilib repositories, the non-multilib
    packages (``i386`` packages which shoudn't be part of the ``x86_64``
    repository) are deleted from the directory.
parameters:
  rpmdir:
    required: true
    description: absolute or relative path to the directory containing RPM files
      (the task directory is considered the current working directory)
    type: str
  arch:
    required: true
    description: list of architectures for which to create YUM metadata
    type: list of str
  outdir:
    required: false
    description: absolute or relative path to an output directory, where
      repository should be created. If not specified, then ``rpmdir`` is used
      as an output directory. ``outdir`` will be created automatically if not
      present.
    type: str
    default: None
  dodelta:
    required: false
    description: whether to create delta RPMs during the mash process
    type: bool
    default: False
    choices: [True, False]
returns: |
  (``string``) path to the directory where the repository has been created
raises: |
  * :class:`.TaskotronDirectiveError`: if ``createrepo`` returns a non-zero exit
    code
version_added: 0.4
"""

EXAMPLES = """
First, download all required RPMs, then create a YUM repository in that
directory::

    - name: download koji tag
      koji:
          action: download_tag
          koji_tag: ${koji_tag}
          arch: ${arch}
          target_dir: "${workdir}/downloaded_tag/"

    - name: mash downloaded rpms
      mash:
          rpmdir: "${workdir}/downloaded_tag/"
          dodelta: False
          arch: ${arch}
"""

import subprocess as sub
import os
import mash
import mash.config
import StringIO
from ConfigParser import RawConfigParser

from libtaskotron.directives import BaseDirective
from libtaskotron.logger import log
from libtaskotron.exceptions import TaskotronDirectiveError
import libtaskotron.file_utils as file_utils

directive_class = 'MashDirective'


class MashDirective(BaseDirective):
    def do_mash(self, rpmdir, dodelta, arch, outdir=None):
        """Set up a mash object with an ad-hoc config

        :param str rpmdir: directory containing rpms to mash
        :param bool dodelta: create drpms during mash
        :param str arch: arch of the rpms
        :param str outdir: output directory where repo is created,
                           if not specified or None, ``rpmdir`` is used.
                           ``outdir`` will be created automatically if
                           not present.
        :rtype str
        :returns: path to directory where repo was created
        """

        # copied over from the old depcheck
        distname = 'depcheck'
        mash_conf_items = ["[%s]" % distname,
                           "output_dir = ",
                           "output_subdir = ",
                           "rpm_path = %s" % (rpmdir if not outdir else outdir),
                           "strict_keys = False",
                           "multilib = True",
                           "multilib_method = devel",
                           "tag = this-is-a-dummy-tag",
                           "inherit = False",
                           "debuginfo = False",
                           "delta = %s" % dodelta,
                           "source = False",
                           "arches = %s" % " ".join(arch),
                           "keys = ",]
        mash_conf = '\n'.join(mash_conf_items)

        conf = mash.config.readMainConfig('/etc/mash/mash.conf')
        dist = mash.config.MashDistroConfig()
        parser = RawConfigParser()

        parser.readfp(StringIO.StringIO(mash_conf))
        dist.populate(parser, distname, conf)
        conf.distros.append(dist)
        dist.repodata_path = dist.rpm_path
        dist.name = distname

        themash = mash.Mash(dist)
        themash.logger.handlers = [themash.logger.handlers[-1]]

        log.info('running createrepo on %s', rpmdir)
        if outdir:
            p = sub.Popen(['createrepo', '-o', outdir, rpmdir], stdout=sub.PIPE,
                          stderr=sub.PIPE)
            for rpm in os.listdir(rpmdir):
                os.symlink(os.path.join(rpmdir, rpm), os.path.join(outdir, rpm))
        else:
            p = sub.Popen(['createrepo', rpmdir], stdout=sub.PIPE,
                          stderr=sub.PIPE)

        output, errors = p.communicate()

        if p.returncode:
            raise TaskotronDirectiveError(errors)

        log.info('running mash multilib on %s', rpmdir)
        themash.doMultilib()
        conf.distros.pop()

        return rpmdir if not outdir else outdir

    def process(self, params, arg_data):
        if 'rpmdir' not in params or 'arch' not in params:
            detected_args = ', '.join(params.keys())
            raise TaskotronDirectiveError(
                "The mash directive requires 'rpmdir' and 'arch' "
                "arguments. Detected arguments: %s" % detected_args)

        if 'dodelta' not in params:
            dodelta = False
        else:
            dodelta = params['dodelta']

        rpmdir = params['rpmdir']
        outdir = params.get('outdir', None)
        arch = params['arch']

        # create outdir if it doesn't exist
        if outdir:
            try:
                file_utils.makedirs(outdir)
            except OSError, e:
                log.exception("Can't create directory: %s", outdir)
                raise TaskotronDirectiveError(e)

        return self.do_mash(rpmdir, dodelta, arch, outdir)
