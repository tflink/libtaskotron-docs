# -*- coding: utf-8 -*-
# Copyright 2009-2016, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import

import os
import subprocess

from libtaskotron.directives import BaseDirective
from libtaskotron.exceptions import TaskotronDirectiveError
from libtaskotron.logger import log
from libtaskotron import os_utils

DOCUMENTATION = """
module: shell_directive
short_description: execute one ore more shell commands or direct processes

description: |
  Execute a command in shell or as a direct process. There are no parameters for this directive,
  it uses a slightly different format:

  - Provide a list of commands directly under the directive name, each item is one command.
  - If you use a string, it will be executed in the default shell (including all shell expansions).
  - If you use a list of strings, it will be executed as a direct process (no shell expansions
    apply, which also means you don't need to worry about escaping special characters).
  - If start the string with ``ignorereturn:`` key and create a list sub-section under it, all
    those commands will ignore their exit code and not raise an error.

  See the example section for a better idea.

  This directive is intended for basic simple scripting or running commands. If you need anything
  more advanced, it's recommended to create a standalone script (bash, perl, etc) and execute it
  instead.
returns: |
  The stdout of executed shell command, in case of multiple commands, output of the last one is
  returned.
raises: |
  * :class:`.TaskotronDirectiveError`: if shell directive does not consist of a list of commands;
    if command is not a string or a list of strings; if command to be executed does not exist; if
    command returned non-zero returncode (returncode can be ignored, see examples).
version_added: 0.4.15
"""

EXAMPLES = """
This executes the shell command in subprocess::

  - name: pull docker image
    shell:
        - docker pull httpd

You can also execute multiple commands::

  - name: do some stuff
    shell:
        - docker pull httpd
        - docker run -d httpd
        - docker ps

Shell directive will raise if command (or one of them if multiple) returns non-zero returncode,
you can bypass this by using ``ignorereturn``::

  - name: do more stuff
    shell:
        - ignorereturn:
            - docker pull thiswontwork!1!1!!
        - echo 'No prob, continuing'

You can run a process directly, without shell processing, which means you don't need to worry
about escaping special characters or e.g. spaces in file names::

  - name: execute command directly, without shell
    shell:
        - [echo, don't worry about code injection]
        - [rm, a file with * funny & characters]
        # the apostrophes are here just to make YAML syntax valid, they don't get passed in
        - [mkdir, '${variable_with_potentially_unsafe_contents}']

You can also run bash script::

  - name: execute bash script
    shell:
        - bash test.sh
    export: test_output

Shell directive can be used to copy file contents into a variable. With this, you can later use
``${loaded_file}`` in other directives::

  - name: load file to formula variable
    shell:
        - cat some_file
    export: loaded_file
"""

directive_class = 'ShellDirective'


class ShellDirective(BaseDirective):
    def process(self, params, arg_data, ignorereturn=False):

        if not isinstance(params, list):
            log.debug('Shell directive received invalid input: %r', params)
            raise TaskotronDirectiveError("Shell directive must contain a list of commands")

        basedir = os.path.dirname(os.path.abspath(arg_data['task']))
        output = ""

        for cmd in params:
            if isinstance(cmd, dict) and 'ignorereturn' in cmd:
                output = self.process(cmd['ignorereturn'], arg_data, ignorereturn=True)
                continue

            try:
                if isinstance(cmd, list) and all([isinstance(c, basestring) for c in cmd]):
                    log.info("Executing process: %s", cmd)
                    output, _ = os_utils.popen_rt(cmd, cwd=basedir)
                elif isinstance(cmd, basestring):
                    log.info("Executing shell: %s", cmd)
                    output, _ = os_utils.popen_rt(cmd, shell=True, cwd=basedir)
                else:
                    cmd_type = [type(c) for c in cmd] if isinstance(cmd, list) else type(cmd)
                    raise TaskotronDirectiveError("Command %r is in invalid type %r, use string or"
                                                  " list of strings" % (cmd, cmd_type))

            except subprocess.CalledProcessError as e:
                if not ignorereturn:
                    raise TaskotronDirectiveError(e)
            except OSError as e:
                raise TaskotronDirectiveError(e)

        return output
