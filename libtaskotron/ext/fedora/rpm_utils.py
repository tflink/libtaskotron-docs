# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

''' Utility methods related to RPM '''

from __future__ import absolute_import
import re
import hawkey
import subprocess
import pipes

from libtaskotron.logger import log
from libtaskotron import exceptions as exc
from libtaskotron import os_utils


def rpmformat(rpmstr, fmt='nvr', end_arch=False):
    '''
    Parse and convert an RPM package version string into a different format.
    String identifiers: N - name, E - epoch, V - version, R - release, A -
    architecture.

    :param str rpmstr: string to be manipulated in a format of N(E)VR
                       (``foo-1.2-3.fc20`` or ``bar-4:1.2-3.fc20``) or N(E)VRA
                       (``foo-1.2-3.fc20.x86_64`` or ``bar-4:1.2-3.fc20.i686``)
    :param str fmt: desired format of the string to be returned. Allowed options
                    are: ``nvr``, ``nevr``, ``nvra``, ``nevra``, ``n``, ``e``,
                    ``v``, ``r``, ``a``. If arch is not present in ``rpmstr``
                    but requested in ``fmt``, ``noarch`` is used. Epoch is
                    provided only when specifically requested (e.g.
                    ``fmt='nevr'``) **and** being non-zero; otherwise it's
                    supressed (the only exception is ``fmt='e'``, where you
                    receive ``0`` for zero epoch).
    :param bool end_arch: set this to ``True`` if ``rpmstr`` ends with an
                          architecture identifier (``foo-1.2-3.fc20.x86_64``).
                          It's not possible to reliably distinguish that case
                          automatically.
    :return: string based on the specified format, or integer if ``fmt='e'``
    :raise TaskotronValueError: if ``fmt`` value is not supported
    '''
    fmt = fmt.lower()
    supported_formats = ['nvr', 'nevr', 'nvra', 'nevra',
                         'n', 'e', 'v', 'r', 'a']
    if fmt not in supported_formats:
        raise exc.TaskotronValueError("Format '%s' not in supported formats "
                                      "(%s)" % (fmt, ', '.join(supported_formats)))

    # add arch if not present
    if not end_arch:
        rpmstr += '.noarch'

    # split rpmstr
    nevra = hawkey.split_nevra(rpmstr)

    # return simple fmt
    if len(fmt) == 1:
        return {'n': nevra.name,
                'e': nevra.epoch,
                'v': nevra.version,
                'r': nevra.release,
                'a': nevra.arch}[fmt]

    # return complex fmt
    evr = nevra.evr()
    # supress epoch if appropriate
    if 'e' not in fmt or nevra.epoch == 0:
        evr = evr[evr.find(':')+1:]  # remove 'epoch:' from the beginning

    result = '%s-%s' % (nevra.name, evr)

    # append arch if requested
    if 'a' in fmt:
        result += '.' + nevra.arch

    return result


def cmpNEVR(nevr1, nevr2):
    '''Compare two RPM version identifiers in NEVR format.

    :param str nevr1: RPM identifier in N(E)VR format
    :param str nevr2: RPM identifier in N(E)VR format
    :return: ``-1``/``0``/``1`` if ``nevr1 < nevr2`` / ``nevr1 == nevr2`` /
             ``nevr1 > nevr2``
    :rtype: int
    :raise TaskotronValueError: if name in ``nevr1`` doesn't match name in
                                ``nevr2``
    '''
    rpmver1 = hawkey.split_nevra(nevr1 + '.noarch')
    rpmver2 = hawkey.split_nevra(nevr2 + '.noarch')

    if rpmver1.name != rpmver2.name:
        raise exc.TaskotronValueError("Name in nevr1 doesn't match name in "
                                      "nevr2: %s, %s" % (nevr1, nevr2))

    # sack is needed for the comparison, because it can be influence the
    # comparison (for example epoch can be set to be ignored). A default empty
    # sack should match Fedora customs
    sack = hawkey.Sack()

    # we need evr_cmp to return int so we can use it as comparison function
    # in python's sorted
    return int(rpmver1.evr_cmp(rpmver2, sack))


def install(pkgs):
    '''Install packages from system repositories using DNF. Either root or sudo access required.

    Note: This is first tested with ``--cacheonly`` to avoid downloading metadata and speed up the
    process. If that command fails for whatever reason, we run it again, this time without
    ``--cacheonly``.

    :param pkgs: packages to be installed, e.g. ``['pidgin']``, or any other argument supported by
                 ``dnf install`` command
    :type pkgs: list of str
    :raise TaskotronPermissionError: if we don't have permissions to run DNF as admin
    :raise TaskotronError: if DNF return code is not zero
    '''
    if not pkgs:
        return

    log.info('Installing %d packages...', len(pkgs))
    pkglist = ' '.join([pipes.quote(pkg) for pkg in pkgs])

    if not os_utils.is_root() and not os_utils.has_sudo():
        raise exc.TaskotronPermissionError("Can't install packages without root or sudo access. "
                                           'Packages requested: %s' % pkglist)

    cmd = ['dnf', '--assumeyes', 'install']
    cmd.extend(pkgs)

    if dnf_cache_available():
        cmd.insert(1, '--cacheonly')
    else:
        log.warn('No DNF cache available, metadata will be need to be downloaded. If your DNF '
                 "cache doesn't persist across task executions (e.g. on a disposable VM), "
                 'consider creating the cache in your base system image to speed up execution.')

    if not os_utils.is_root():  # we must have sudo at this point, don't test again needlessly
        cmd = ['sudo', '--non-interactive'] + cmd

    while True:  # we need to call the command twice, if the first run with --cacheonly fails
        log.debug('Running: %s', ' '.join([pipes.quote(c) for c in cmd]))
        try:
            output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            if '--cacheonly' in cmd:  # maybe just outdated cache
                log.debug(u'✘ Package installation failed, DNF output was:\n%s', e.output.rstrip())
                log.debug("Trying again with forced metadata refresh...")
                cmd = list(cmd)  # make a copy to allow unit tests to intercept both calls easily
                cmd.remove('--cacheonly')
                cmd.append('--refresh')
                continue

            log.error(u'✘ Package installation failed. We tried to install following packages:\n%s'
                      '\nDNF returned exit code %d and output:\n%s', pkglist, e.returncode,
                      e.output.rstrip())
            raise exc.TaskotronError("Unable to install packages: %s" % pkglist)
        else:
            log.debug(u'✔ Package installation completed successfully. DNF output was:\n%s',
                      output.rstrip())
            return

        # safeguard against infinite loop, we should never reach this code
        log.critical("Infinite loop detected in 'install()'")
        assert False, "Infinite loop detected in 'install()'"
        return


def is_installed(pkgs):
    '''Check if packages are installed using DNF. No elevated permissions needed.

    :param pkgs: packages to be checked whether they are installed, e.g. ``['pidgin']`` or any
                 other argument supported by ``dnf install`` command
    :type pkgs: list of str
    :return: True if all specified packages are installed, False otherwise
    :rtype: bool
    '''
    if not pkgs:
        return True

    log.info('Checking installed state of %d packages...', len(pkgs))
    cmd = ['dnf', '--assumeno', '--disableplugin=noroot', 'install']
    cmd.extend(pkgs)

    if dnf_cache_available():
        cmd.insert(1, '--cacheonly')
    else:
        log.warn('No DNF cache available, metadata will be need to be downloaded. If your DNF '
                 "cache doesn't persist across task executions (e.g. on a disposable VM), "
                 'consider creating the cache in your base system image to speed up execution.')

    if not os_utils.is_root() and os_utils.has_sudo():
        cmd = ['sudo', '--non-interactive'] + cmd

    log.debug('Running: %s', ' '.join([pipes.quote(c) for c in cmd]))
    try:
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        log.debug(u'✘ Some of specified packages are not installed. DNF returned exit code %d and '
                  'output:\n%s', e.returncode, e.output.rstrip())
        return False
    else:
        log.debug(u'✔ All specified packages are installed')
        return True


def dnf_cache_available():
    '''Determine whether DNF cache is available.

    If it is, we can run read-only actions solely from the cache, and try to use it even for
    ``install`` action (provided the repos haven't changed much). This way we avoid contacting
    DNF repos and downloading new metadata if they changed.

    This tests the system cache (if running as root or with sudo access) or the user cache
    (otherwise). Since DNF has no direct way of testing this, we simply run an action that should
    always pass (``dnf repolist``) with ``--cacheonly`` option and look at the exit code.

    :return: bool whether DNF cache is currently available and can be used (system or user cache,
             depending on your current admin privileges)
    '''
    cmd = ['dnf', '--cacheonly', 'repolist']

    if not os_utils.is_root() and os_utils.has_sudo():
        cmd = ['sudo', '--non-interactive'] + cmd

    log.debug('Deciding whether DNF cache is available. Running: %s', ' '.join(cmd))
    try:
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        log.debug(u'✘ DNF cache is not available. Received exit code %d and output:\n%s',
                  e.returncode, e.output.rstrip())
        return False
    else:
        log.debug(u'✔ DNF cache is available.')
        return True


def get_dist_tag(rpmstr):
    '''Parse disttag from an RPM package version string.

    :param str rpmstr: string to be manipulated in a format of N(E)VR
                       (``foo-1.2-3.fc20`` or ``bar-4:1.2-3.fc20``) or N(E)VRA
                       (``foo-1.2-3.fc20.x86_64`` or ``bar-4:1.2-3.fc20.i686``)
    :return: string containing dist tag (``fc20``)
    :raise TaskotronValueError: if ``rpmstr`` does not contain dist tag
    '''

    release = rpmformat(rpmstr, 'r')
    matches = re.findall(r'\.(fc\d{1,2})\.?', release)
    if not matches:
        raise exc.TaskotronValueError('Could not parse disttag from %s' % rpmstr)

    # there might be e.g. fc22 in git commit hash as part of the release,
    # so just take the last match which should be disttag
    return matches[-1]
